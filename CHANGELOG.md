# [2.3.0](https://gitlab.com/Mark-Partola/semantic-release-test/compare/v2.2.4...v2.3.0) (2023-10-04)


### Bug Fixes

* **ci:** fix ([eb94ed4](https://gitlab.com/Mark-Partola/semantic-release-test/commit/eb94ed4edd4a1061ce1562da7ff8f2d4aa57a95e))
* **ci:** fix branch ([383b475](https://gitlab.com/Mark-Partola/semantic-release-test/commit/383b475ef61ad455de2cf446174319125c8f22f1))


### Features

* **add rule:** rule ([4276c7e](https://gitlab.com/Mark-Partola/semantic-release-test/commit/4276c7ec41cc383963724679fb319a6a9f530dbf))

## [2.2.4](https://gitlab.com/Mark-Partola/semantic-release-test/compare/v2.2.3...v2.2.4) (2023-10-04)


### Bug Fixes

* **fix:** commit changelog ([fd56433](https://gitlab.com/Mark-Partola/semantic-release-test/commit/fd56433fd11e702cac6ab1137a58a339c0e2fbed))
